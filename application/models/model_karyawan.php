<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_karyawan extends Ci_Model {

     function getdata(){
            $query=$this->db->get('tb_karyawan')->result();
            return $query;
     }

     function savedata($param){
         $query=$this->db->insert('tb_karyawan',$param);
         return $query;
     }
 
     function updatedata($param,$id){
        $this->db->where('nik',$id);
        $query=$this->db->update('tb_karyawan',$param);
        return $query;
    }

    function deletedata($id){
        $this->db->where('nik',$id);
        $query=$this->db->delete('tb_karyawan');
        return $query;
    }

    function finddata($id=null){
        $this->db->where('nik',$id);
        $query=$this->db->get('tb_karyawan')->result();
        return $query;
    }
}
